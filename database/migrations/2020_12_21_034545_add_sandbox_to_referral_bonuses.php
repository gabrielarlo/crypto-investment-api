<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSandboxToReferralBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('referral_bonuses', 'sandbox')) {
            Schema::table('referral_bonuses', function (Blueprint $table) {
                $table->integer('sandbox')->default(0);
            });
        }
        if (!Schema::hasColumn('encashments', 'sandbox')) {
            Schema::table('encashments', function (Blueprint $table) {
                $table->integer('sandbox')->default(0);
            });
        }
        if (!Schema::hasColumn('referrals', 'sandbox')) {
            Schema::table('referrals', function (Blueprint $table) {
                $table->integer('sandbox')->default(0);
            });
        }
        if (!Schema::hasColumn('wallets', 'sandbox')) {
            Schema::table('wallets', function (Blueprint $table) {
                $table->integer('sandbox')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referral_bonuses', function (Blueprint $table) {
            //
        });
    }
}

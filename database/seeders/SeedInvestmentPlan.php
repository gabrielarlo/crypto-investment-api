<?php

namespace Database\Seeders;

use App\Models\InvestmentPlan;
use Illuminate\Database\Seeder;

class SeedInvestmentPlan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => 'basic',
                'duration' => 90,
                'interest' => 1,
            ],
            [
                'name' => 'bronze',
                'duration' => 120,
                'interest' => 1.33,
            ],
            [
                'name' => 'silver',
                'duration' => 240,
                'interest' => 1.5,
            ],
            [
                'name' => 'gold',
                'duration' => 360,
                'interest' => 1.67,
            ],
        ];

        foreach ($plans as $plan) {
            InvestmentPlan::firstOrCreate(['name' => $plan['name']], ['duration' => $plan['duration'], 'interest' => $plan['interest']]);
        }
    }
}

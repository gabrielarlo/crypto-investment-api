<?php

namespace Database\Seeders;

use App\Models\Investment;
use Illuminate\Database\Seeder;

class SeedInvestmentIID extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = Investment::where('iid', null)->orWhere('iid', '')->get();
        $int = 0;
        foreach ($i as $item) {
            $newI = Investment::find($item->id);
            $newI->iid = encode(now()->timestamp + $int, 'investment-iid');
            $newI->save();
            $int++;
        }
    }
}

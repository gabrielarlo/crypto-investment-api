<?php

namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SeedDefaultSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::firstOrCreate(['key' => 'daily_payout'], ['val' => '1']);
        Settings::firstOrCreate(['key' => 'weekly_payout'], ['val' => '1']);
    }
}

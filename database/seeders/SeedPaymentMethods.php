<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class SeedPaymentMethods extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::firstOrCreate(['name' => 'EOS', 'image_name' => 'eos-logo'], ['label' => 'EOS Deposit Wallet Address', 'description' => 'Send payment on this wallet address and upload your screenshot as proof of transaction including reference id. Note: Send ONLY EOS to this address or else it will be lost.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'Bitcoin', 'image_name' => 'btc-logo'], ['label' => 'BTC Deposit Wallet Address', 'description' => 'Send payment on this wallet address and upload your screenshot as proof of transaction including reference id. Note: Send ONLY BTC to this address or else it will be lost.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'GCash', 'image_name' => 'gcash-logo'], ['label' => 'GCash Deposit Number', 'description' => 'Send payment on this number and upload your screenshot as proof of transaction including reference id.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'Coins.ph', 'image_name' => 'coins-ph-logo'], ['label' => 'CoinsPH Deposit Account', 'description' => 'Send payment on this account and upload your screenshot as proof of transaction including reference id.', 'account' => '00020101021127740012com.p2pqrpay0111DCPHPHM1XXX0208999644030411974301200330512+974301200335204601653036085802PH5912Arlo Gabriel6005Davao63041D3E']);
        PaymentMethod::firstOrCreate(['name' => 'UnionBank', 'image_name' => 'ub-logo'], ['label' => 'Unionbank Deposit Account', 'description' => 'Send payment on this account and upload your screenshot as proof of transaction including reference id.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'BDO', 'image_name' => 'bdo-logo'], ['label' => 'BDO Deposit Account', 'description' => 'Send payment on this account and upload your screenshot as proof of transaction including reference id.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'PayMaya', 'image_name' => 'paymaya-logo'], ['label' => 'PayMaya Deposit Number', 'description' => 'Send payment on this number and upload your screenshot as proof of transaction including reference id.', 'account' => '']);
        PaymentMethod::firstOrCreate(['name' => 'Paypal', 'image_name' => 'paypal-logo'], ['label' => 'PayPal Email Address', 'description' => 'Send payment on this email address and upload your screenshot as proof of transaction including reference id.', 'account' => '']);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Market;
use Illuminate\Database\Seeder;

class SeedMarkets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coins = [
            [
                'coin' => 'PTS',
                'price' => 0.01,
                'pair' => 'USD',
                'charge' => 3,
            ],
            [
                'coin' => 'PTS',
                'price' => 0.50,
                'pair' => 'PHP',
                'charge' => 10,
            ],
        ];

        foreach ($coins as $coin) {
            Market::firstOrCreate(['coin' => $coin['coin'], 'pair' => $coin['pair']], ['price' => $coin['price'], 'charge_percentage' => $coin['charge']]);
        }
    }
}

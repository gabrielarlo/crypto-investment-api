@component('mail::message')
# New Withdrawal Request

Good day! Please check the app for new withdrawal request of *{{ $user->name }}*,

@component('mail::button', ['url' => config('app.url') . '/payouts'])
    Go to app
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent

@component('mail::message')
# New Deposit Request

Hooray! Please check the app for new deposit request of *{{ $user->name }}*,

@component('mail::button', ['url' => config('app.url') . '/deposits'])
Go to app
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent

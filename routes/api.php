<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\InvestmentController;
use App\Http\Controllers\API\LedgerController;
use App\Http\Controllers\API\MarketController;
use App\Http\Controllers\API\PaymentController;
use App\Http\Controllers\API\ReferralController;
use App\Http\Controllers\API\SandboxController;
use App\Http\Controllers\API\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/', function () {
    return res('API v1 is running...');
});

Route::get('/unauthenticated', function () {
    return res('Unauthenticated', null, 401);
})->name('unauthenticated');

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/check-referrer-wallet', [AuthController::class, 'checkReferrerWallet']);
    Route::post('/verify-token', [AuthController::class, 'verifyToken']);
    Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('/new-password', [AuthController::class, 'newPasswords']);
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/change-password', [AuthController::class, 'changePassword']);
        Route::post('/change-avatar', [AuthController::class, 'changeAvatar']);
        Route::post('/activate-account', [AuthController::class, 'activateAccount']);
        Route::post('/ban-action', [AuthController::class, 'banAction']);
        Route::post('/check-auth', function () {
            return res('You are authenticated...');
        });
    });

    Route::group(['prefix' => 'wallet'], function () {
        Route::post('/balance', [WalletController::class, 'balance']);
        Route::post('/convert', [WalletController::class, 'convert']);
        Route::post('/send', [WalletController::class, 'send']);
        Route::post('/top-up', [WalletController::class, 'topUp']);
        Route::post('/withdraw', [WalletController::class, 'withdraw']);
        Route::post('/complete-transaction', [WalletController::class, 'completeTransaction']);
        Route::post('/setup', [WalletController::class, 'setup']);
        Route::post('/history', [WalletController::class, 'history']);
        Route::post('/withdrawal-history', [WalletController::class, 'withdrawalHistory']);
        Route::post('/pending-deposits', [WalletController::class, 'pendingDeposits']);
        Route::post('/pending-withdrawals', [WalletController::class, 'pendingWithdrawals']);
        Route::post('/pts-ledger', [WalletController::class, 'ptsLedger']);
        Route::post('/take-action', [WalletController::class, 'takeAction']);
        Route::post('/get-minimum-to-withdraw', [WalletController::class, 'getMinimumToWithdraw']);
    });

    Route::group(['prefix' => 'investment'], function () {
        Route::post('/invest', [InvestmentController::class, 'invest']);
        Route::post('/stats', [InvestmentController::class, 'stats']);
        Route::post('/history', [InvestmentController::class, 'history']);
        Route::post('/all-history', [InvestmentController::class, 'allHistory']);
        Route::post('/all-investors', [InvestmentController::class, 'allInvestors']);
        Route::post('/change-status', [InvestmentController::class, 'changeStatus']);
        Route::post('/plan-list', [InvestmentController::class, 'planList']);
        Route::post('/change-payout-status', [InvestmentController::class, 'changePayoutStatus']);
    });

    Route::group(['prefix' => 'ledger'], function () {
        Route::post('/list', [LedgerController::class, 'list']);
    });

    Route::group(['prefix' => 'dashboard'], function () {
        Route::post('/my-dashboard', [DashboardController::class, 'myDashboard']);
        Route::post('/admin-dashboard', [DashboardController::class, 'adminDashboard']);
    });

    Route::group(['prefix' => 'market'], function () {
        Route::post('/price', [MarketController::class, 'price']);
    });

    Route::group(['prefix' => 'referral'], function () {
        Route::post('/list', [ReferralController::class, 'list']);
        Route::post('/my-list', [ReferralController::class, 'myList']);
    });

    Route::group(['prefix' => 'sandbox'], function () {
        Route::post('/list', [SandboxController::class, 'list']);
        Route::post('/create', [SandboxController::class, 'create']);
        Route::post('/remove', [SandboxController::class, 'remove']);
        Route::post('/details', [SandboxController::class, 'details']);
        Route::post('/add-fund', [SandboxController::class, 'addFund']);
        Route::post('/change-password', [SandboxController::class, 'changePassword']);
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::post('/list', [PaymentController::class, 'list']);
    });
});


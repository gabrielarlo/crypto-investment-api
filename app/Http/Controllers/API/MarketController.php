<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\MarketInterface;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    protected $market;

    public function __construct(MarketInterface $market)
    {
        $this->market = $market;
    }

    public function buy()
    {
        return $this->market->buy(\request());
    }

    public function sell()
    {
        return $this->market->sell(\request());
    }

    public function trade()
    {
        return $this->market->trade(\request());
    }

    public function price()
    {
        return $this->market->price(\request());
    }
}

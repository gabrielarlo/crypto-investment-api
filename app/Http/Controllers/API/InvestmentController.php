<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\InvestmentInterface;
use Illuminate\Http\Request;

class InvestmentController extends Controller
{
    protected $investment;

    public function __construct(InvestmentInterface $investment)
    {
        $this->investment = $investment;
    }

    public function invest()
    {
        return $this->investment->invest(\request());
    }

    public function stats()
    {
        return $this->investment->stats(\request());
    }

    public function history()
    {
        return $this->investment->history(\request());
    }

    public function allHistory()
    {
        return $this->investment->allHistory(\request());
    }

    public function allInvestors()
    {
        return $this->investment->allInvestors(\request());
    }

    public function changeStatus()
    {
        return $this->investment->changeStatus(\request());
    }

    public function planList()
    {
        return $this->investment->planList(\request());
    }

    public function changePayoutStatus()
    {
        return $this->investment->changePayoutStatus(\request());
    }
}

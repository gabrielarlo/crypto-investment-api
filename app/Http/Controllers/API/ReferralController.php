<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\ReferralInterface;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    protected $referral;

    public function __construct(ReferralInterface $referral)
    {
        $this->referral = $referral;
    }

    public function list()
    {
        return $this->referral->allList(\request());
    }

    public function myList()
    {
        return $this->referral->myList(\request());
    }
}

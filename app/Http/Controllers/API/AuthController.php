<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\AuthInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
    }

    public function login()
    {
        return $this->auth->login(\request());
    }

    public function logout()
    {
        return $this->auth->logout(\request());
    }

    public function register()
    {
        return $this->auth->register(\request());
    }

    public function checkReferrerWallet()
    {
        return $this->auth->checkReferrerWallet(\request());
    }

    public function verifyToken()
    {
        return $this->auth->verifyToken(\request());
    }

    public function forgotPassword()
    {
        return $this->auth->forgotPassword(\request());
    }

    public function newPasswords()
    {
        return $this->auth->newPasswords(\request());
    }

    public function changePassword()
    {
        return $this->auth->changePassword(\request());
    }

    public function changeAvatar()
    {
        return $this->auth->changeAvatar(\request());
    }

    public function activateAccount()
    {
        return $this->auth->activateAccount(\request());
    }

    public function banAction()
    {
        return $this->auth->banAction(\request());
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\SandboxInterface;
use Illuminate\Http\Request;

class SandboxController extends Controller
{
    protected $sandbox;

    public function __construct(SandboxInterface $sandbox)
    {
        $this->sandbox = $sandbox;
    }

    public function list()
    {
        return $this->sandbox->list(\request());
    }

    public function details()
    {
        return $this->sandbox->details(\request());
    }

    public function create()
    {
        return $this->sandbox->create(\request());
    }

    public function remove()
    {
        return $this->sandbox->remove(\request());
    }

    public function addFund()
    {
        return $this->sandbox->addFund(\request());
    }

    public function changePassword()
    {
        return $this->sandbox->changePassword(\request());
    }
}

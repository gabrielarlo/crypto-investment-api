<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\WalletInterface;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    protected $wallet;

    public function __construct(WalletInterface $wallet)
    {
        $this->wallet = $wallet;
    }

    public function balance()
    {
        return $this->wallet->balance();
    }

    public function convert()
    {
        return $this->wallet->convert(\request());
    }

    public function send()
    {
        return $this->wallet->send(\request());
    }

    public function topUp()
    {
        return $this->wallet->topUp(\request());
    }

    public function withdraw()
    {
        return $this->wallet->withdraw(\request());
    }

    public function completeTransaction()
    {
        return $this->wallet->completeTransaction(\request());
    }

    public function setPassword()
    {
        return $this->wallet->setPassword(\request());
    }

    public function changePassword()
    {
        return $this->wallet->changePassword(\request());
    }

    public function setup()
    {
        return $this->wallet->setup(\request());
    }

    public function history()
    {
        return $this->wallet->history(\request());
    }

    public function withdrawalHistory()
    {
        return $this->wallet->withdrawalHistory(\request());
    }

    public function pendingDeposits()
    {
        return $this->wallet->pendingDeposits(\request());
    }

    public function pendingWithdrawals()
    {
        return $this->wallet->pendingWithdrawals(\request());
    }

    public function ptsLedger()
    {
        return $this->wallet->ptsLedger(\request());
    }

    public function takeAction()
    {
        return $this->wallet->takeAction(\request());
    }

    public function getMinimumToWithdraw()
    {
        return $this->wallet->getMinimumToWithdraw(\request());
    }
}

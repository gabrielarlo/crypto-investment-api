<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\DashboardInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $dashboard;

    public function __construct(DashboardInterface $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function myDashboard()
    {
        return $this->dashboard->myDashboard(\request());
    }

    public function adminDashboard()
    {
        return $this->dashboard->adminDashboard(\request());
    }
}

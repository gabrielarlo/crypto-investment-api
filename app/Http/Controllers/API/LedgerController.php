<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\LedgerInterface;
use Illuminate\Http\Request;

class LedgerController extends Controller
{
    protected $ledger;

    public function __construct(LedgerInterface $ledger)
    {
        $this->ledger = $ledger;
    }

    public function list()
    {
        return $this->ledger->list(\request());
    }
}

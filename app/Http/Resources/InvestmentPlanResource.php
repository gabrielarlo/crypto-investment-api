<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvestmentPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'duration' => $this->duration,
            'interest' => $this->interest,
            'active' => $this->status == 1,
            'daily_payout' => getSettings('daily_payout') == '1',
            'weekly_payout' => getSettings('weekly_payout') == '1',
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Interfaces\WalletInterface;
use App\Repositories\WalletRepository;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $balance = (new WalletRepository(null, null))->balance(true, $this->id);

        return [
            'hashid' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'email' => $this->email,
            'username' => $this->username,
            'wallet_address' => $this->wallet_address,
            'referrer' => $this->referrer != null ? $this->referrer->name : '',
            'created_at' => $this->created_at->toString(),
            'active' => $this->status == 1,
            'ban' => $this->ban == 1,
            'invested' => $this->investments->count() > 0,
            'available' => $balance['available'],
            'top-up' => $balance['top-up'],
            'on-hold' => $balance['on-hold'],
            'sandbox' => $this->sandbox == 1,
            'avatar' => getImage('avatar', $this->id),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Carbon\CarbonImmutable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class InvestmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        $week = 0;
        $month = 0;
        $progress = $this->progress();
        $interest = $this->interest;
        if ($progress > 0) {
            $weekStartDate = CarbonImmutable::now()->startOfWeek();
            $includedWeekDays = now()->diffInDays($weekStartDate);

            $monthStartDate = CarbonImmutable::now()->startOfMonth();
            $includedMonthDays = now()->diffInDays($monthStartDate);

            $released = $this->encashmentBalance->released;
            $perDay = $released / $progress;

            if ($includedWeekDays >= $progress) {
                $week = $interest;
            } else {
                $toLess = ($progress - $includedWeekDays) * $perDay;
                $week = $interest - $toLess;
            }

            if ($includedMonthDays >= $progress) {
                $month = $interest;
            } else {
                $toLess = ($progress - $includedMonthDays) * $perDay;
                $month = $interest - $toLess;
            }
        }

        return [
            'iid' => $this->iid,
            'plan' => $this->plan->name,
            'duration' => $this->plan->duration,
            'progress' => $progress,
            'capital' => $this->capital,
            'profit' => $interest,
            'total' => $this->interest + $this->capital,
            'status' => $this->statusText(),
            'created' => $this->created_at->toString(),
            'expiration' => now()->addDays($this->plan->duration - $this->progress())->toString(),
            'week' => $week,
            'month' => $month,
        ];
    }
}

<?php

namespace App\Http\Resources;

use App\Abstracts\LedgerStatus;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;
use function Symfony\Component\String\s;

class LedgerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = 'pending';
        switch ($this->status) {
            case LedgerStatus::PROCESSING:
                $status = 'processing';
                break;
            case LedgerStatus::FAILED:
                $status = 'failed';
                break;
            case LedgerStatus::SUCCESS:
                $status = 'success';
                break;
        }

        return [
            'user_wallet_address' => $this->user->wallet_address,
            'confirmed_by' => $this->confirm != null ? $this->confirm->name : '',
            'transaction_id' => $this->trans_id,
            'flow' => $this->flow == 1 ? 'deposit' : 'withdraw',
            'amount' => $this->amount,
            'currency' => $this->currency,
            'method' => $this->method,
            'reference' => $this->reference,
            'status' => $status,
            'created_at' => $this->created_at->toString(),
            'updated_at' => $this->updated_at->toString(),
            'image' => getImage('receipt', $this->trans_id),
        ];
    }
}

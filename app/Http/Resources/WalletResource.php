<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;

class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = 'pending';
        switch ($this->status) {
            case 1:
                $status = 'in-progress';
                break;
            case 2:
                $status = 'failed';
                break;
            case 3:
                $status = 'success';
                break;
        }

        $image = null;
        if (File::exists(public_path('receipt/' . hash('sha256', $this->trans_id) . '.jpg'))) {
            $image = url('/receipt/' . hash('sha256', $this->trans_id) . '.jpg');
        }

        return [
            'hashid' => encode($this->id, 'uuid'),
            'trans_id' => $this->trans_id,
            'trans_type' => $this->trans_type,
            'amount' => $this->amount,
            'wallet_address' => $this->wallet_address,
            'remarks' => $this->remarks,
            'status' => $status,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->toString(),
            'sandbox' => $this->sandbox == 1,
            'image' => $image,
        ];
    }
}

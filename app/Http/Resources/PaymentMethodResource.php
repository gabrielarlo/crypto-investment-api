<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentMethodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'image-name' => $this->image_name,
            'label' => $this->label,
            'description' => $this->description,
            'account' => $this->account,
            'active' => $this->status == 1,
        ];
    }
}

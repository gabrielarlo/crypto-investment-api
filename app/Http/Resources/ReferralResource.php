<?php

namespace App\Http\Resources;

use App\Models\Investment;
use App\Models\InvestmentPlan;
use App\Repositories\InvestmentStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class ReferralResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $plans = ['basic', 'bronze', 'silver', 'gold'];
        $items = [
            'basic' => false,
            'bronze' => false,
            'silver' => false,
            'gold' => false,
        ];

        foreach ($plans as $plan) {
            $p = InvestmentPlan::where('name', $plan)->first();
            $has = Investment::where('user_id', $this->referral_id)->where('plan_id', $p->id)->where('status', '!=', 3)->count();
            $items[$plan] = $has > 0;
        }


        return [
            'name' => $this->referral->name,
            'joined_date' => $this->referral->created_at->toString(),
            'wallet_address' => $this->referral->wallet_address,
            'has-basic' => $items['basic'],
            'has-bronze' => $items['bronze'],
            'has-silver' => $items['silver'],
            'has-gold' => $items['gold'],
        ];
    }
}

<?php


namespace App\Repositories;


use App\Http\Resources\UserResource;
use App\Interfaces\AuthInterface;
use App\Interfaces\SandboxInterface;
use App\Interfaces\WalletInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SandboxRepository implements SandboxInterface
{
    protected $wallet;
    protected $auth;

    public function __construct(WalletInterface $wallet, AuthInterface $auth)
    {
        $this->wallet = $wallet;
        $this->auth = $auth;
    }

    public function list($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $pager = pager($req);
        $page = $pager['page'];
        $count = User::where('status', 1)->where('ban', 0)->where('sandbox', 1)->count();
        $u = User::where('status', 1)->where('ban', 0)->where('sandbox', 1)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = UserResource::collection($u);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function create($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        // Inside are the rest of validations
        $req['sandbox'] = 1;
        return $this->auth->register($req);
    }

    public function remove($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $u = User::where('id', $uid)->where('sandbox', 1)->first();
        if (!$u) return eRes('Sandbox Account not found');

        if ($u->sandbox == 0) return eRes('Use only for sandbox account');

        $u->status = 0;
        $u->save();

        return res('Success');
    }

    public function details($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $u = User::where('id', $uid)->where('sandbox', 1)->first();
        if (!$u) return eRes('Sandbox Account not found');

        if ($u->sandbox == 0) return eRes('Use only for sandbox account');

        $data = new UserResource($u);

        return res('Success', $data);
    }

    public function addFund($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'wallet_address' => 'required',
            'amount' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('wallet_address', $req->wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$u) return eRes('Invalid wallet address');

        if ($u->sandbox == 0) return eRes('Use only for sandbox account');

        $this->wallet->addSandboxFund($req->amount, $req->wallet_address);

        return res('Success');
    }

    public function changePassword($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'password' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $u = User::find($uid);
        if (!$u) return eRes('Sandbox Account not found');

        if ($u->sandbox == 0) return eRes('Use only for sandbox account');

        $u->password = bcrypt($req->password);
        $u->save();

        return res('Success');
    }
}

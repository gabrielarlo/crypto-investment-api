<?php


namespace App\Repositories;

use App\Abstracts\FloatingFund;
use App\Abstracts\MinimumWithdraw;
use App\Abstracts\TransactionStatus;
use App\Abstracts\TransactionType;
use App\Http\Resources\WalletResource;
use App\Interfaces\LedgerInterface;
use App\Interfaces\MarketInterface;
use App\Interfaces\WalletInterface;
use App\Mail\NewDeposits;
use App\Mail\NewWithdrawal;
use App\Models\Investment;
use App\Models\ReferralBonus;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class WalletRepository implements WalletInterface
{
    protected $market;
    protected $ledger;

    public function __construct(?MarketInterface $market, ?LedgerInterface $ledger)
    {
        $this->market = $market;
        $this->ledger = $ledger;
    }

    public function balance(bool $toArray = false, ?int $user_id = null)
    {
        $uid = $user_id ?? Auth::user()->id;

        $flowIn = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::EARNED)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $flowOut = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::CONVERTED)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $flowOut += Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->sum('amount');

        $topUpIn = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $topUpIn += Wallet::where('user_id', $uid)->where('trans_type', TransactionType::RECEIVEDTOPUP)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $topUpOut = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::INVESTED)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $topUpOut += Wallet::where('user_id', $uid)->where('trans_type', TransactionType::SENTTOPUP)->where('status', TransactionStatus::SUCCESS)->sum('amount');

        $referralBonus = ReferralBonus::where('user_id', $uid)->where('status', 1)->sum('received');

        $onHold = Investment::where('user_id', $uid)->get()->sum('on_hold_total');

        $available = $flowIn - $flowOut;
        $available += $referralBonus;
        $topUp = $topUpIn - $topUpOut;

        $balance = [
            'available' => $available,
            'top-up' => $topUp,
            'on-hold' => $onHold,
        ];
        if ($toArray) return $balance;

        return res('Success', $balance);
    }

    public function convert($req)
    {
        $v = Validator::make($req->all(), [
            'amount' => 'required',
            'wallet_password' => 'required|min:6',
        ]);
        if ($v->fails()) return vRes($v->errors());

        if ($req->amount <= 0) return eRes('Invalid amount');

        if (!$this->checkPassword($req->wallet_password)) return eRes('Invalid wallet password');

        $balance = $this->balance(true);
        if ($balance['available'] < $req->amount) return eRes('No enough funds');

        $transId = $this->burnIn(Auth::user()->id, TransactionType::CONVERTED, $req->amount, '', '');
        if ($transId == null) return eRes('Error W001');

        $amount = $req->amount - ($req->amount * 0.05);
        $otherTransId = $this->burnIn(Auth::user()->id, TransactionType::TOPUP, $amount, '', 'From Conversion at ' . $transId);
        if ($otherTransId == null) return eRes('Error W002');

        $this->changeStatus($transId, TransactionStatus::SUCCESS);
        $this->changeStatus($otherTransId, TransactionStatus::SUCCESS);

        return res('Success', $transId);
    }

    public function send($req)
    {
        $v = Validator::make($req->all(), [
            'wallet_address' => 'required|min:24',
            'amount' => 'required',
            'wallet_password' => 'required|min:6',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $amount = str_replace(',', '', $req->amount);

        if ($amount <= 0) return eRes('Invalid amount');

        if (!$this->checkPassword($req->wallet_password)) return eRes('Invalid wallet password');

        $balance = $this->balance(true);
        $floatingFund = $this->market->converter('PHP', 'PTS', FloatingFund::PHP);
        $topUp = $balance['top-up'] - $floatingFund;
        if ($topUp < $amount) return eRes('No enough funds');

        $receiver = User::where('wallet_address', $req->wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$receiver) return eRes('Invalid receiver wallet address');

        $transId = $this->burnIn(Auth::user()->id, TransactionType::SENTTOPUP, $req->amount, $req->wallet_address, '');
        if ($transId == null) return eRes('Error W001');

        $otherTransId = $this->burnIn($receiver->id, TransactionType::RECEIVEDTOPUP, $req->amount, Auth::user()->wallet_address, '');
        if ($otherTransId == null) return eRes('Error W002');

        $this->changeStatus($transId, TransactionStatus::SUCCESS);
        $this->changeStatus($otherTransId, TransactionStatus::SUCCESS);

        return res('Success', $transId);
    }

    public function topUp($req)
    {
        $v = Validator::make($req->all(), [
            'amount' => 'required',
            'currency' => 'required',
            'quantity' => 'required',
            'method' => 'required',
            'reference_no' => 'required',
            'imageFile' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        if ($req->amount <= 0) return eRes('Invalid amount');

        $remarks = "Method: {$req->method}, Ref No: {$req->reference_no}, Amount: {$req->amount} {$req->currency}";

        $transId = $this->burnIn(Auth::user()->id, TransactionType::TOPUP, $req->quantity, '', $remarks);
        if ($transId == null) return eRes('Error W001');

        $this->changeStatus($transId, TransactionStatus::PROCESSING);

        $this->ledger->deposit($req, $transId);

        saveImage($req->imageFile, 'receipt', hash('sha256', $transId));

        Mail::to(config('mail.admin'))->send(new NewDeposits(Auth::user()));

        return res('Success', $transId);
    }

    public function withdraw($req)
    {
        $v = Validator::make($req->all(), [
            'amount' => 'required',
            'currency' => 'required',
            'quantity' => 'required',
            'method' => 'required',
            'withdraw_details' => 'required',
            'wallet_password' => 'required|min:6',
        ]);
        if ($v->fails()) return vRes($v->errors());

        if ($req->amount <= 0) return eRes('Invalid amount');

        if (!$this->checkPassword($req->wallet_password)) return eRes('Invalid wallet password');

        $quantity = str_replace(',', '', $req->quantity);

        $balance = $this->balance(true);
        $floatingFund = $this->market->converter('PHP', 'PTS', FloatingFund::PHP);
        $minimumWithdraw = $this->market->converter('PHP', 'PTS', MinimumWithdraw::PHP);
        $available = $balance['available'] - ($floatingFund + $minimumWithdraw);
        if ($available < $quantity) return eRes('No enough funds to withdraw');
        $quantity -= $floatingFund;

        $remarks = "Method: {$req->method}, Deposit Account: {$req->withdraw_details}";
        $transId = $this->burnIn(Auth::user()->id, TransactionType::WITHDRAW, $quantity, '', $remarks);
        if ($transId == null) return eRes('Error W001');

        $this->changeStatus($transId, TransactionStatus::PROCESSING);

        $this->ledger->withdraw($req, $transId);

        Mail::to(config('mail.admin'))->send(new NewWithdrawal(Auth::user()));

        return res('Success', $transId);
    }

    public function completeTransaction($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'transaction_id' => 'required',
            'remarks' => 'required',
        ]);
        if ($v->fails()) return ($v->errors());

        $remarks = "Extra: {$req->remarks}";
        $result = $this->changeStatus($req->transaction_id, TransactionStatus::SUCCESS, $remarks);
        if (!$result) return eRes('Error 1');

        $result = $this->ledger->completeTransaction($req->transaction_id, $req->remarks);
        if (!$result) return eRes('Error 2');

        return res('Success');
    }

    public function setPassword($req)
    {
        $v = Validator::make($req->all(), [
            'password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::find(Auth::user()->id);
        $u->wallet_password = bcrypt($req->password);
        $u->save();

        return res('Success');
    }

    public function changePassword($req)
    {
        $v = Validator::make($req->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::find(Auth::user()->id);
        if (!Hash::check($req->current_password, $u->wallet_password)) return eRes('Invalid current password');

        $u->wallet_password = bcrypt($req->password);
        $u->save();

        return res('Success');
    }

    public function earn($amount, string $wallet_address, string $plan, string $InvestmentId): bool
    {
        $earner = User::where('wallet_address', $wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$earner) return false;

        $remarks = "From {$plan} plan ({$InvestmentId})";
        $transId = $this->burnIn($earner->id, TransactionType::EARNED, $amount, '', $remarks, true);
        if ($transId == null) return false;

        return true;
    }

    public function sendToInvestment($amount, String $plan, int $encashment = 1): bool
    {
        $remarks = "For investing {$plan} plan encashed per {$encashment} day(s).";
        $transId = $this->burnIn(Auth::user()->id, TransactionType::INVESTED, $amount, '', $remarks, true);
        if ($transId == null) return false;

        return true;
    }

    public function setup($req)
    {
        $uid = Auth::user()->id;
        $u = User::find($uid);
        if ($u->wallet_password == null) {
            return res('No Password', ['done' => false]);
        }
        return res('Completed', ['done' => true]);
    }

    public function history($req)
    {
        $pager = pager($req);
        $page = $pager['page'];

        $uid = Auth::user()->id;
        if ($req->has('user_id')) $uid = $req->user_id;

        $count = Wallet::where('user_id', $uid)->count();
        $w = Wallet::where('user_id', $uid)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = WalletResource::collection($w);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function withdrawalHistory($req)
    {
        $pager = pager($req);
        $page = $pager['page'];
        $uid = Auth::user()->id;
        if ($req->has('user_id')) $uid = $req->user_id;

        $count = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->count();
        $wh = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = WalletResource::collection($wh);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function addSandboxFund($amount, string $wallet_address): bool
    {
        $u = User::where('wallet_address', $wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$u) return false;

        if ($u->sandbox == 0) return false;

        $au = User::find(Auth::user()->id);
        $remarks = 'Send by Admin through BTC';
        $transId = $this->burnIn($u->id, TransactionType::TOPUP, $amount, $au->wallet_address, $remarks, true);
        if ($transId == null) return false;

        return true;
    }

    public function pendingDeposits($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $pager = pager($req);
        $page = $pager['page'];
        $count = Wallet::where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::PROCESSING)->count();
        $w = Wallet::where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::PROCESSING)->orderBy('created_at', 'desc')->where('sandbox', 0)->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = WalletResource::collection($w);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function pendingWithdrawals($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $pager = pager($req);
        $page = $pager['page'];
        $count = Wallet::where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::PROCESSING)->where('sandbox', 0)->count();
        $w = Wallet::where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::PROCESSING)->where('sandbox', 0)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = WalletResource::collection($w);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function ptsLedger($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $pager = pager($req);
        $page = $pager['page'];
        $count = Wallet::where('sandbox', 0)->count();
        $w = Wallet::where('sandbox', 0)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = WalletResource::collection($w);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function takeAction($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
            'action' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $w = Wallet::find($uid);
        if (!$w) return eRes('Invalid transaction');

        if ($req->action == 'accept') {
            $remarks = "Extra: Accepted by admin via system";
            $result = $this->changeStatus($w->trans_id, TransactionStatus::SUCCESS, $remarks);
            if (!$result) return eRes('Error 1');

            $result = $this->ledger->completeTransaction($w->trans_id, $remarks);
            if (!$result) return eRes('Error 2');
        } else {
            $remarks = "Extra: Declined by admin via system";
            $result = $this->changeStatus($w->trans_id, TransactionStatus::FAILED, $remarks);
            if (!$result) return eRes('Error 3');

            $result = $this->ledger->completeTransaction($w->trans_id, $remarks, false);
            if (!$result) return eRes('Error 4');
        }

        return res('Success');
    }

    public function getMinimumToWithdraw($req)
    {
        $v = Validator::make($req->all(), [
            'currency' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $floating = strtolower($req->currency) == 'php' ? FloatingFund::PHP : FloatingFund::USD;
        $minimum = strtolower($req->currency) == 'php' ? MinimumWithdraw::PHP : MinimumWithdraw::USD;

        return res('Success', [
            'floating' => $floating,
            'minimum' => $minimum,
        ]);
    }

    private function checkPassword($password)
    {
        $u = User::find(Auth::user()->id);
        if ($u->wallet_password == null) {
            $u->wallet_password = bcrypt($password);
            $u->save();

            return true;
        }
        return Hash::check($password, $u->wallet_password);
    }

    private function burnIn(int $actor_id, string $type, string $amount, string $other_wallet_address = '', string $remarks = '', bool $setSuccess = false): ?string
    {
        $actor = User::find($actor_id);
        if (!$actor) return null;

        $flow = 0;
        if ($type == TransactionType::TOPUP || $type == TransactionType::RECEIVEDTOPUP) {
            $flow = 1;
        }

        if ($other_wallet_address == '') {
            $other_wallet_address = $actor->wallet_address;
        }

        $other_user = User::where('wallet_address', $other_wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$other_user) return false;

        // Check for duplication
        $transId = encode(now()->timestamp, 'transaction');
        $cw = Wallet::where('trans_id', $transId)->where('trans_type', $type)->where('amount', $amount)->first();
        if ($cw) return $cw->trans_id;

        // Same TransID produced
        $sw = Wallet::where('trans_id', $transId)->first();
        if ($sw) {
            $transId = encode(now()->timestamp + rand(1, 9999), 'transaction');
        }

        $sandbox = 0;
        if ($actor->sandbox == 1 || $other_user->sandbox == 1) {
            $sandbox = 1;
        }

        $w = new Wallet();
        $w->user_id = $actor->id;
        $w->trans_id = $transId;
        $w->trans_type = $type;
        $w->flow = $flow;
        $w->amount = $amount;
        $w->wallet_address = $other_wallet_address;
        $w->remarks = $remarks;
        $w->status = $setSuccess ? TransactionStatus::SUCCESS : TransactionStatus::PENDING;
        $w->sandbox = $sandbox;
        $w->save();

        return $transId;
    }

    private function changeStatus(string $transId, int $status, string $remarks = '')
    {
        $w = Wallet::where('trans_id', $transId)->first();
        if (!$w) return false;

        $newRemarks = $w->remarks;
        if ($remarks != '') {
            $newRemarks = "{$newRemarks}, {$remarks}";
        }
        $w->status = $status;
        $w->remarks = $newRemarks;
        $w->save();

        return true;
    }
}

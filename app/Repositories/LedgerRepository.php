<?php


namespace App\Repositories;


use App\Abstracts\LedgerStatus;
use App\Http\Resources\LedgerResource;
use App\Interfaces\LedgerInterface;
use App\Models\Ledger;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LedgerRepository implements LedgerInterface
{

    public function list($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $pager = pager($req);
        $page = $pager['page'];
        $count = Ledger::count();
        $d = Ledger::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = LedgerResource::collection($d);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function deposit($req, string $transId)
    {
        $u = User::find(Auth::user()->id);
        if ($u->sandbox == 0) {
            $v = Validator::make($req->all(), [
                'amount' => 'required',
                'method' => 'required',
                'currency' => 'required',
                'reference_no' => 'required',
            ]);
            if ($v->fails()) return false;

            $l = new Ledger();
            $l->user_id = $u->id;
            $l->trans_id = $transId;
            $l->flow = 1;
            $l->amount = $req->amount;
            $l->currency = $req->currency;
            $l->method = $req->method;
            $l->reference = $req->reference_no;
            $l->status = LedgerStatus::PROCESSING;
            $l->confirmed_by = 0;
            $l->save();
        }

        return true;
    }

    public function withdraw($req, string $transId)
    {
        $u = User::find(Auth::user()->id);
        if ($u->sandbox == 0) {
            $v = Validator::make($req->all(), [
                'amount' => 'required',
                'currency' => 'required',
                'method' => 'required',
            ]);
            if ($v->fails()) return false;

            $l = new Ledger();
            $l->user_id = $u->id;
            $l->trans_id = $transId;
            $l->flow = 0;
            $l->amount = $req->amount;
            $l->currency = $req->currency;
            $l->method = $req->method;
            $l->status = LedgerStatus::PROCESSING;
            $l->confirmed_by = 0;
            $l->save();
        }

        return true;
    }

    public function completeTransaction(string $transId, string $refNo, bool $setSuccess = true)
    {
        $l = Ledger::where('trans_id', $transId)->first();
        if (!$l) return false;

        $l->confirmed_by = Auth::user()->id;
        $l->reference = "{$l->reference},  {$refNo}";
        $l->status = $setSuccess ? LedgerStatus::SUCCESS : LedgerStatus::FAILED;
        $l->save();

        return true;
    }
}

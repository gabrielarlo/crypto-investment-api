<?php


namespace App\Repositories;


use App\Http\Resources\ReferralResource;
use App\Interfaces\ReferralInterface;
use App\Models\Referrals;
use Illuminate\Support\Facades\Auth;

class ReferralRepository implements ReferralInterface
{

    public function allList($req)
    {
        $pager = pager($req);
        $page = $pager['page'];
        $count = Referrals::where('status', 1)->count();
        $r = Referrals::where('status', 1)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = ReferralResource::collection($r);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function myList($req)
    {
        $pager = pager($req);
        $page = $pager['page'];
        $count = Referrals::where('referrer_id', Auth::user()->id)->where('status', 1)->count();
        $r = Referrals::where('referrer_id', Auth::user()->id)->where('status', 1)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();

        $list = ReferralResource::collection($r);
        return res('Success', compact('count', 'list', 'page'));
    }
}

<?php


namespace App\Repositories;


use App\Interfaces\AuthInterface;
use App\Mail\AccountVerification;
use App\Mail\ForgotPassword;
use App\Models\OauthAccessToken;
use App\Models\Referrals;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class AuthRepository implements AuthInterface
{

    public function login($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('username', $req->username)->orWhere('email', $req->username)->where('status', 1)->first();
        if (!$u) return eRes('Invalid username or password');

        if (!Hash::check($req->password, $u->password)) return eRes('Invalid password');

        $data = $this->generateToken($u);

        return res('Success', $data);
    }

    public function logout($req): JsonResponse
    {
        $id = Auth::user()->id;
        $origin = session('origin');
        OauthAccessToken::where('user_id', $id)->where('name', $origin)->delete();

        return res('Success');
    }

    public function register($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('email', $req->email)->first();
        if ($u) return eRes('Email address is already used');

        $username = $req->email;
        $username = substr($username, 0, strpos($username, '@'));
        $onChecking = true;
        while ($onChecking) {
            $u = User::where('username', $username)->first();
            if (!$u) {
                $onChecking = false;
            } else {
                $username = $username . (string)random_int(0, 9);
            }
        }

        $u = new User();
        $u->name = $req->name;
        $u->username = $username;
        $u->email = $req->email;
        $u->password = bcrypt($req->password);
        $u->ban = 0;
        if (config('app.auto_activate')) {
            $u->status = 1;
        } else {
            $u->status = 0;
            $u->email_token = (string)random_int(100000, 999999);
            if (!$req->has('sandbox')) {
                Mail::to($u)->send(new AccountVerification($u));
            }
        }

        $u->referrer_id = 0;
        $hasReferrer = false;
        if ($req->has('referrer_wallet_address')) {
            $ru = User::where('wallet_address', $req->referrer_wallet_address)->where('status', 1)->where('ban', 0)->first();
            if ($ru) {
                $u->referrer_id = $ru->id;
                $hasReferrer = true;
            }
        }
        $u->wallet_address = 'PTS' . encode(now()->timestamp, 'wallet-address');

        if ($req->has('sandbox')) {
            $u->sandbox = $req->sandbox;
            $u->status = 1;
            $hasReferrer = false;
        }

        $u->save();

        if ($hasReferrer) {
            $r = new Referrals();
            $r->referrer_id = $ru->id;
            $r->referral_id = $u->id;
            $r->save();
        }

        return res('Success', [
            'activation-needed' => !config('app.auto_activate'),
        ]);
    }

    public function checkReferrerWallet($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'referrer_wallet_address' => 'required'
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('wallet_address', $req->referrer_wallet_address)->where('status', 1)->where('ban', 0)->first();
        if (!$u) return eRes('Invalid wallet address');
        return res('Success');
    }

    public function verifyToken($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('email_token', $req->token)->first();
        if (!$u) return eRes('Invalid token');

        $u->status = 1;
        $u->email_token = null;
        $u->save();

        return res('Success');
    }

    public function forgotPassword($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'email' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('email', $req->email)->where('status', 1)->where('ban', 0)->first();
        if (!$u) return eRes('Invalid email address');

        $u->email_token = (string)random_int(100000, 999999);
        $u->save();

        Mail::to($u)->send(new ForgotPassword($u));

        return res('Check your email for instructions');
    }

    public function newPasswords($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'token' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $u = User::where('email_token', $req->token)->where('status', 1)->where('ban', 0)->first();
        if (!$u) return eRes('Invalid token');

        $u->password = bcrypt($req->password);
        $u->email_token = null;
        $u->save();

        return res('Success');
    }

    public function changePassword($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'current_password' => 'required',
            'new_password' => 'required|min:6|confirmed',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = Auth::user()->id;
        $u = User::find($uid);
        if (!Hash::check($req->current_password, $u->password)) return eRes('Invalid current password');
        $u->password = bcrypt($req->new_password);
        $u->save();

        return res('Success');
    }

    public function changeAvatar($req): JsonResponse
    {
        $v = Validator::make($req->all(), [
            'imageFile' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $uid = Auth::user()->id;
        saveImage($req->imageFile, 'avatar', hash('sha256', $uid));

        return res('Success');
    }

    public function activateAccount($req): JsonResponse
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so.');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $u = User::find($uid);
        if (!$u) return eRes('User not found');

        if ($u->ban == 1) return eRes('User is banned');
        if ($u->status == 1) return res('Account is already active');

        $u->status = 1;
        $u->save();

        return res('Success');
    }

    public function banAction($req): JsonResponse
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so.');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) vRes($v->errors());

        $uid = decode($req->hashid, 'uuid');
        $u = User::find($uid);
        if (!$u) return eRes('User not found');

        $u->ban = $u->ban == 1 ? 0 : 1;
        $u->save();

        return res($u->ban == 1 ? 'Account successfully banned' : 'Account is successfully unbanned');
    }

    private function generateToken(User $user): array {
        $origin = session('origin');
        OauthAccessToken::where('user_id', $user->id)->where('name', $origin)->delete();
        $role = 'investor';
        switch ($user->role) {
            case 1:
                $role = 'builder';
                break;
            case 9:
                $role = 'admin';
                break;
        }

        if (File::exists('avatar/' . hash('sha256', $user->id) . '.jpg')) {
            $avatar = Image::make('avatar/' . hash('sha256', $user->id) . '.jpg')->encode('data-url', 75)->encoded;
        } else {
            $avatar = Image::make('avatar.jpg')->encode('data-url', 75)->encoded;
        }

        $data = [
            'token' => $user->createToken($origin)->accessToken,
            'user' => [
                'hashid' => encode($user->id, 'uuid'),
                'name' => $user->name,
                'email' => $user->email,
                'username' => $user->username,
                'wallet_address' => $user->wallet_address,
                'avatar' => str_replace('data:image/jpeg;base64,', '', $avatar),
                'role' => $role,
                'created_at' => $user->created_at->toString(),
                'updated_at' => $user->updated_at->toString(),
                'sandbox' => $user->sandbox == 1,
            ],
        ];
        return $data;
    }
}

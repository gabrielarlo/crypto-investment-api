<?php


namespace App\Repositories;


use App\Interfaces\MarketInterface;
use App\Models\Market;
use Illuminate\Support\Facades\Validator;

class MarketRepository implements MarketInterface
{

    public function buy($req)
    {
        // TODO: Implement buy() method.
    }

    public function sell($req)
    {
        // TODO: Implement sell() method.
    }

    public function trade($req)
    {
        // TODO: Implement trade() method.
    }

    public function price($req)
    {
        $v = Validator::make($req->all(), [
            'from' => 'required',
            'to' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $amount = $req->has('amount') ? $req->amount : 1;
        $price = $this->converter($req->from, $req->to, $amount);

        return res('Success', $price);
    }

    public function converter($from, $to, $amount = 1)
    {
        $price = 0;

        $m = Market::where('coin', strtoupper($from))->where('pair', strtoupper($to))->where('status', 1)->first();
        if ($m) {
            $price = $m->price * $amount;
            $price = $price - ($price * ($m->charge_percentage / 100));
        } else {
            $m = Market::where('coin', strtoupper($to))->where('pair', strtoupper($from))->where('status', 1)->first();
            if ($m) {
                $price = $amount / $m->price;
            }
        }

        return $price;
    }
}

<?php


namespace App\Repositories;


use App\Abstracts\InvestmentStatus;
use App\Abstracts\TransactionStatus;
use App\Abstracts\TransactionType;
use App\Http\Resources\InvestmentResource;
use App\Http\Resources\UserResource;
use App\Interfaces\DashboardInterface;
use App\Interfaces\MarketInterface;
use App\Interfaces\WalletInterface;
use App\Models\Investment;
use App\Models\InvestmentPlan;
use App\Models\Ledger;
use App\Models\ReferralBonus;
use App\Models\User;
use App\Models\Wallet;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Auth;

class DashboardRepository implements DashboardInterface
{
    protected $wallet;
    protected $market;

    public function __construct(WalletInterface $wallet, MarketInterface $market) {
        $this->wallet = $wallet;
        $this->market = $market;
    }

    public function myDashboard($req)
    {
        $uid = Auth::user()->id;

        $totalDeposit = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $totalWithdraw = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->sum('amount');
        $totalReferralBonus = ReferralBonus::where('user_id', $uid)->where('status', 1)->sum('received');

        $weekStartDate = CarbonImmutable::now()->startOfWeek();
        $weekDeposit = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $weekStartDate)->sum('amount');
        $weekWithdraw = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $weekStartDate)->sum('amount');
        $weekReferralBonus = ReferralBonus::where('user_id', $uid)->where('status', 1)->where('created_at', '>=', $weekStartDate)->sum('received');

        $monthStartDate = CarbonImmutable::now()->startOfMonth();
        $monthDeposit = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $monthStartDate)->sum('amount');
        $monthWithdraw = Wallet::where('user_id', $uid)->where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $monthStartDate)->sum('amount');
        $monthReferralBonus = ReferralBonus::where('user_id', $uid)->where('status', 1)->where('created_at', '>=', $monthStartDate)->sum('received');

        $balance = $this->wallet->balance(true, $uid);

        $available = $balance['available'];
        $topUp = $balance['top-up'];
        $onHold = $balance['on-hold'];

        $i = Investment::where('user_id', $uid)->where('status', '!=', InvestmentStatus::COMPLETE)->get();
        $investments = InvestmentResource::collection($i);

        $req['from'] = 'PTS';
        $req['to'] = 'USD';
        $req['amount'] = $available;
        $price = $this->market->price($req);
        $usdValue = $price->getData(true)['data'];

        return res('Success', [
            'usd-value' => $usdValue,
            'total-deposit' => $totalDeposit,
            'week-deposit' => $weekDeposit,
            'month-deposit' => $monthDeposit,
            'total-withdraw' => $totalWithdraw,
            'week-withdraw' => $weekWithdraw,
            'month-withdraw' => $monthWithdraw,
            'total-referral-bonus' => $totalReferralBonus,
            'week-referral-bonus' => $weekReferralBonus,
            'month-referral-bonus' => $monthReferralBonus,
            'available' => $available,
            'top-up' => $topUp,
            'on-hold' => $onHold,
            'investments' => $investments,
        ]);
    }

    public function adminDashboard($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed');

        $weekStartDate = CarbonImmutable::now()->startOfWeek();
        $monthStartDate = CarbonImmutable::now()->startOfMonth();

        $bought = [
            'total' => Wallet::where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->where('sandbox', 0)->sum('amount'),
            'week' => Wallet::where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $weekStartDate)->where('sandbox', 0)->sum('amount'),
            'month' => Wallet::where('trans_type', TransactionType::TOPUP)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $monthStartDate)->where('sandbox', 0)->sum('amount'),
        ];
        $sold = [
            'total' => Wallet::where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->where('sandbox', 0)->sum('amount'),
            'week' => Wallet::where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $weekStartDate)->where('sandbox', 0)->sum('amount'),
            'month' => Wallet::where('trans_type', TransactionType::WITHDRAW)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $monthStartDate)->where('sandbox', 0)->sum('amount'),
        ];
        $invested = [
            'total' => Wallet::where('trans_type', TransactionType::INVESTED)->where('status', TransactionStatus::SUCCESS)->where('sandbox', 0)->sum('amount'),
            'week' => Wallet::where('trans_type', TransactionType::INVESTED)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $weekStartDate)->where('sandbox', 0)->sum('amount'),
            'month' => Wallet::where('trans_type', TransactionType::INVESTED)->where('status', TransactionStatus::SUCCESS)->where('created_at', '>=', $monthStartDate)->where('sandbox', 0)->sum('amount'),
        ];
        $interests = [
            'total' => Investment::where('sandbox', 0)->sum('interest'),
            'week' => Investment::where('created_at', '>=', $weekStartDate)->where('sandbox', 0)->sum('interest'),
            'month' => Investment::where('created_at', '>=', $monthStartDate)->where('sandbox', 0)->sum('interest'),
        ];

        $usdPrices = [
            'buy' => 1 / $this->market->converter('usd', 'pts'),
            'sell' => $this->market->converter('pts', 'usd'),
        ];
        $phpPrices = [
            'buy' => 1 / $this->market->converter('php', 'pts'),
            'sell' => $this->market->converter('pts', 'php'),
        ];

        $deposits = [
            'usd' => Ledger::where('flow', 1)->where('currency', 'USD')->where('status', 3)->sum('amount'),
            'php' => Ledger::where('flow', 1)->where('currency', 'PHP')->where('status', 3)->sum('amount'),
        ];
        $withdrawals = [
            'usd' => Ledger::where('flow', 0)->where('currency', 'USD')->where('status', 3)->sum('amount'),
            'php' => Ledger::where('flow', 0)->where('currency', 'PHP')->where('status', 3)->sum('amount'),
        ];

        $investments = [];
        $ip = InvestmentPlan::where('status', 1)->get();
        foreach ($ip as $item) {
            $investments[$item->name] = [
                'total' => Investment::where('plan_id', $item->id)->where('status', '!=', 0)->where('sandbox', 0)->sum('interest'),
                'week' => Investment::where('plan_id', $item->id)->where('status', '!=', 0)->where('created_at', '>=', $weekStartDate)->where('sandbox', 0)->sum('interest'),
                'month' => Investment::where('plan_id', $item->id)->where('status', '!=', 0)->where('created_at', '>=', $monthStartDate)->where('sandbox', 0)->sum('interest'),
            ];
        }

        $earlyDate = now()->subDays(1);
        $members = [
            'all' => User::where('status', 1)->where('ban', 0)->where('sandbox', 0)->count(),
            'invested' => User::where('status', 1)->where('ban', 0)->whereHas('investments')->where('sandbox', 0)->count(),
            'new' => User::where('status', 1)->where('ban', 0)->where('created_at', '>=', $earlyDate)->where('sandbox', 0)->count(),
        ];

        $lm = User::where('status', 1)->where('ban', 0)->orderBy('created_at', 'desc')->limit(5)->where('sandbox', 0)->get();
        $latest_members = UserResource::collection($lm);

        return res('Success', [
            'bought' => $bought,
            'sold' => $sold,
            'invested' => $invested,
            'interests' => $interests,
            'usd-prices' => $usdPrices,
            'php-prices' => $phpPrices,
            'deposits' => $deposits,
            'withdrawals' => $withdrawals,
            'investments' => $investments,
            'members' => $members,
            'latest-members' => $latest_members,
        ]);
    }
}

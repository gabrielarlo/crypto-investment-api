<?php


namespace App\Repositories;


use App\Http\Resources\PaymentMethodResource;
use App\Interfaces\PaymentInterface;
use App\Models\PaymentMethod;

class PaymentRepository implements PaymentInterface
{

    public function list($req)
    {
        $pm = PaymentMethod::all();
        $data = PaymentMethodResource::collection($pm);

        return res('Success', $data);
    }
}

<?php


namespace App\Repositories;


use App\Abstracts\InvestmentStatus;
use App\Http\Resources\InvestmentPlanResource;
use App\Http\Resources\InvestmentResource;
use App\Http\Resources\UserResource;
use App\Interfaces\InvestmentInterface;
use App\Interfaces\WalletInterface;
use App\Models\Encashment;
use App\Models\Investment;
use App\Models\InvestmentPlan;
use App\Models\ReferralBonus;
use App\Models\Settings;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InvestmentRepository implements InvestmentInterface
{
    protected $wallet;

    public function __construct(?WalletInterface $wallet) {
        $this->wallet = $wallet;
    }

    public function invest($req)
    {
        $v = Validator::make($req->all(), [
            'plan' => 'required',
            'amount' => 'required',
            'encashment_rate' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $p = InvestmentPlan::where('name', $req->plan)->first();
        if (!$p) return eRes('Invalid plan');

        if ($p->status == 0) return eRes('Sorry, there is no available slot for this plan as of the moment.');

        if ($req->encashment_rate == 1) {
            if (getSettings('daily_payout') != '1') return eRes('This payout option has reach out its limit');
        }
        if ($req->encashment_rate == 7) {
            if (getSettings('weekly_payout') != '1') return eRes('This payout option has reach out its limit');
        }

        if ($req->amount <= 0) return eRes('Invalid amount');

        $balance = $this->wallet->balance(true);
        if ($balance['top-up'] < $req->amount) return eRes('Not enough Top-Up funds');

        $this->wallet->sendToInvestment($req->amount, $req->plan, $req->encashment_rate);

        $u = User::find(Auth::user()->id);
        $i = new Investment();
        $i->user_id = $u->id;
        $i->iid = encode(now()->timestamp, 'investment-iid');
        $i->plan_id = $p->id;
        $i->capital = $req->amount;
        $i->interest = 0;
        $i->duration = $p->duration;
        $i->encashment = $req->encashment_rate;
        $i->status = InvestmentStatus::PROGRESS;
        $i->sandbox = $u->sandbox;
        $i->save();

        $e = new Encashment();
        $e->investment_id = $i->id;
        $e->sandbox = $u->sandbox;
        $e->save();

        if ($u->referrer_id > 0) {
            $rb = new ReferralBonus();
            $rb->user_id = $u->referrer_id;
            $rb->investment_id = $i->id;
            $rb->received = 0;
            $rb->sandbox = $u->sandbox;
            $rb->save();
        }

        return res('Success');
    }

    public function stats($req)
    {
        $filter = null;
        if ($req->has('filter')) {
            $filter = $req->filter;
        }

        $uid = Auth::user()->id;
        if ($filter == null || $filter == 0 || $filter > 3) {
            $d = Investment::where('user_id', $uid)->get();
        } else {
            $d = Investment::where('user_id', $uid)->where('status', $filter)->get();
        }

        $data = InvestmentResource::collection($d);
        return res('Success', $data);
    }

    public function process(Investment $investment)
    {
        if ($investment->status == InvestmentStatus::PROGRESS) {
            $rate = $investment->plan->interest / 86400; // per second
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $investment->created_at);
            $seconds = now()->diffInSeconds($start_date);
            $days = now()->diffInDays($start_date);

            $interest = $investment->capital * (($rate * $seconds) / 100);

            $i = Investment::find($investment->id);
            $i->interest = $interest;
            if ($investment->duration <= $days) {
                $i->status = InvestmentStatus::MATURED;
            }
            $i->save();

            // Feeding Encashment
            $current_event = floor($investment->progress() / $investment->encashment);
            $e = Encashment::where('investment_id', $investment->id)->first();
            if ($investment->encashmentBalance->event < $current_event) {
                $this->wallet->earn($e->on_hold, $investment->user->wallet_address, $investment->plan->name, $investment->iid);

                $e->released = $e->released + $e->on_hold;
                $e->on_hold = 0;
                $e->event = $e->event + 1;
            } else {
                $e->on_hold = $interest - $e->released;
            }

            $e->save();

            // Feeding Referral Bonus
            $ou = User::where('id', $investment->user_id)->where('referrer_id', '!=', 0)->first();
            if ($ou) {
                $current_rb_event = floor($investment->progress() / 31) + 1; // To make it every first of 30 days
                $rb = ReferralBonus::where('user_id', $ou->referrer_id)->where('investment_id', $investment->id)->first();
                $received = ($investment->capital * 0.05) * $current_rb_event;
                if ($rb) {
                    if ($rb->event < $current_rb_event) {
                        $rb->received = $received;
                        $rb->event = $current_rb_event;
                        $rb->save();
                    }
                } else {
                    $new_rb = new ReferralBonus();
                    $new_rb->user_id = $ou->referrer_id;
                    $new_rb->investment_id = $investment->id;
                    $new_rb->received = $received;
                    $new_rb->event = $current_event;
                    $new_rb->sandbox = $ou->sandbox;
                    $new_rb->save();
                }
            }
        } elseif ($investment->status == InvestmentStatus::MATURED) {
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $investment->created_at);
            $days = now()->diffInDays($start_date);

            if (($investment->duration + 30) <= $days) {
                $i = Investment::find($investment->id);
                $i->status = InvestmentStatus::COMPLETE;
                $i->save();
            }
        }
    }

    public function history($req)
    {
        $pager = pager($req);
        $page = $pager['page'];
        $uid = Auth::user()->id;
        if ($req->has('user_id')) $uid = $req->user_id;

        $count = Investment::where('user_id', $uid)->count();
        $i = Investment::where('user_id', $uid)->orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->get();
        $list = InvestmentResource::collection($i);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function allHistory($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed');

        $pager = pager($req);
        $page = $pager['page'];
        $count = Investment::where('sandbox', 0)->count();
        $i = Investment::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->where('sandbox', 0)->get();
        $list = InvestmentResource::collection($i);

        return res('Success', compact('count', 'list', 'page'));
    }

    public function allInvestors($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed');

        $pager = pager($req);
        $page = $pager['page'];
        $count = User::where('sandbox', 0)->count();
        $i = User::orderBy('created_at', 'desc')->offset($pager['offset'])->limit($pager['limit'])->where('sandbox', 0)->get();
        $list = UserResource::collection($i);

        return res('Success', compact('count','list', 'page'));
    }

    public function changeStatus($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'hashid' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $id = decode($req->hashid, 'uuid');
        $ip = InvestmentPlan::find($id);
        if (!$ip) return eRes('Plan not found');

        $ip->status = $ip->status == 1 ? 0 : 1;
        $ip->save();

        return res('Success');
    }

    public function planList($req)
    {
        $ip = InvestmentPlan::all();

        $data = InvestmentPlanResource::collection($ip);
        return res('Success', $data);
    }

    public function changePayoutStatus($req)
    {
        if (Auth::user()->role != 9) return eRes('You are not allowed to do so');

        $v = Validator::make($req->all(), [
            'key' => 'required',
        ]);
        if ($v->fails()) return vRes($v->errors());

        $valid_keys = ['daily_payout', 'weekly_payout'];
        if (array_search($req->key, $valid_keys) === false) return eRes('Invalid key 001');

        $s = Settings::where('key', $req->key)->first();
        if (!$s) return eRes('Invalid key 002');

        $s->val = $s->val == '1' ? '0' : '1';
        $s->save();

        return res('Success');
    }
}

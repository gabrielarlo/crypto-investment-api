<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Referrals extends Model
{
    use HasFactory;

    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id');
    }

    public function referral()
    {
        return $this->belongsTo(User::class, 'referral_id');
    }
}

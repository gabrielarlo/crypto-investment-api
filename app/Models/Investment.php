<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    use HasFactory;

    protected $appends = ['on_hold_total'];

    public function plan()
    {
        return $this->belongsTo(InvestmentPlan::class, 'plan_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function encashmentBalance()
    {
        return $this->hasOne(Encashment::class, 'investment_id');
    }

    public function getOnHoldTotalAttribute()
    {
        return $this->hasMany(Encashment::class, 'investment_id')->sum('on_hold');
    }

    public function statusText()
    {
        switch ($this->attributes['status']) {
            case 1:
                return 'in-progress';
            case 2:
                return 'matured';
            case 3:
                return 'complete';
        }
    }

    public function progress()
    {
        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at']);
        return now()->diffInDays($start_date);
    }
}

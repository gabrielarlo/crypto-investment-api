<?php


namespace App\Interfaces;


interface WalletInterface
{
    public function balance(bool $toArray = false, ?int $user_id = null);

    public function convert($req);

    public function send($req);

    public function topUp($req);

    public function withdraw($req);

    public function completeTransaction($req);

    public function setPassword($req);

    public function changePassword($req);

    public function setup($req);

    public function history($req);

    public function withdrawalHistory($req);

    public function sendToInvestment($amount, String $plan, int $encashment = 1): bool;

    public function earn($amount, string $wallet_address, string $plan, string $InvestmentId): bool;

    public function addSandboxFund($amount, string $wallet_address): bool;

    public function pendingDeposits($req);

    public function pendingWithdrawals($req);

    public function ptsLedger($req);

    public function takeAction($req);

    public function getMinimumToWithdraw($req);
}

<?php


namespace App\Interfaces;


interface LedgerInterface
{
    public function list($req);

    public function deposit($req, string $transId);

    public function withdraw($req, string $transId);

    public function completeTransaction(string $transId, string $refNo, bool $setSuccess = true);
}

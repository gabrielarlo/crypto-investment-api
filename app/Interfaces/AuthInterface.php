<?php


namespace App\Interfaces;


use Illuminate\Http\JsonResponse;

interface AuthInterface
{
    public function login($req): JsonResponse;

    public function logout($req): JsonResponse;

    public function register($req): JsonResponse;

    public function checkReferrerWallet($req): JsonResponse;

    public function verifyToken($req): JsonResponse;

    public function forgotPassword($req): JsonResponse;

    public function newPasswords($req): JsonResponse;

    public function changePassword($req): JsonResponse;

    public function changeAvatar($req): JsonResponse;

    public function activateAccount($req): JsonResponse;

    public function banAction($req): JsonResponse;
}

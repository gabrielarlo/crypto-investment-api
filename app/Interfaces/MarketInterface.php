<?php


namespace App\Interfaces;


interface MarketInterface
{
    public function buy($req);

    public function sell($req);

    public function trade($req);

    public function price($req);

    public function converter($from, $to, $amount = 1);
}

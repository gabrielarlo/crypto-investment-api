<?php


namespace App\Interfaces;


interface ReferralInterface
{
    public function allList($req);

    public function myList($req);
}

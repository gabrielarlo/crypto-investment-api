<?php


namespace App\Interfaces;


interface DashboardInterface
{
    public function myDashboard($req);

    public function adminDashboard($req);
}

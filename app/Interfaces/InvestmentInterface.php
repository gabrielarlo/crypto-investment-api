<?php


namespace App\Interfaces;


use App\Models\Investment;

interface InvestmentInterface
{
    public function invest($req);

    public function stats($req);

    public function history($req);

    public function allHistory($req);

    public function allInvestors($req);

    public function process(Investment $investment);

    public function changeStatus($req);

    public function planList($req);

    public function changePayoutStatus($req);
}

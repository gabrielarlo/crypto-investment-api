<?php


namespace App\Interfaces;


interface SandboxInterface
{
    public function list($req);

    public function create($req);

    public function remove($req);

    public function details($req);

    public function addFund($req);

    public function changePassword($req);
}

<?php


namespace App\Abstracts;


abstract class TransactionType {
    const CONVERTED = 'converted';
    const SENTTOPUP = 'sent-topup';
    const RECEIVEDTOPUP = 'received-topup';
    const TOPUP = 'topup';
    const WITHDRAW = 'withdraw';
    const INVESTED = 'invested';
    const EARNED = 'earned';
}

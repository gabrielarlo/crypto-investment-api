<?php


namespace App\Abstracts;


abstract class LedgerStatus {
    const PENDING = 0;
    const PROCESSING = 1;
    const FAILED = 2;
    const SUCCESS = 3;
}

<?php


namespace App\Abstracts;


abstract class InvestmentStatus {
    const PROGRESS = 1;
    const MATURED = 2;
    const COMPLETE = 3;
}

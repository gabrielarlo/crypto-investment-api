<?php

namespace App\Abstracts;

abstract class MinimumWithdraw
{
    const PHP = 1000;
    const USD = 20;
}

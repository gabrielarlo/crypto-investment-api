<?php

use App\Models\Settings;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

function res($msg = '', $data = null, $code = 200)
{
    return response()->json([
        'code' => $code,
        'msg' => $msg,
        'data' => $data,
    ]);
}

function vRes($data = null)
{
    return res('Validation Failed', $data, 412);
}

function eRes($msg = '')
{
    return res($msg, null, 400);
}

function encode($id, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    return (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->encode($id);
}

function decode($hash, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    $decode = (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->decode($hash);
    if (count($decode) > 0) return $decode[0];
    return null;
}

function saveImage(string $base64, string $publicDir, string $name): void
{
    if (!File::exists($publicDir)) File::makeDirectory($publicDir, 0755, true);
    Image::make($base64)->save("{$publicDir}/" . $name . '.jpg', 100, 'jpg');
}

function getImage($type = 'avatar', $id): ?string
{
    $image = null;
    if (File::exists(public_path($type . '/' . hash('sha256', $id) . '.jpg'))) {
        $image = url("/{$type}/" . hash('sha256', $id) . '.jpg');
    }
    return $image;
}

function pager($req, int $limit = 10)
{
    $page = 1;
    if ($req->has('page')) {
        $page = $req->page;
    }
    $offset = ($page - 1) * $limit;
    return [
        'offset' => $offset,
        'limit' => $limit,
        'page' => $page,
    ];
}

function getSettings(string $key): ?string
{
    $s = Settings::where('key', $key)->first();
    if (!$s) return null;
    return $s->val;
}

<?php

namespace App\Console\Commands;

use App\Interfaces\WalletInterface;
use App\Models\Investment;
use App\Repositories\InvestmentRepository;
use Illuminate\Console\Command;

class CalculateInvestments extends Command
{
    protected $wallet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investment:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the interest of investments';

    /**
     * Create a new command instance.
     *
     * @param WalletInterface $wallet
     */
    public function __construct(WalletInterface $wallet)
    {
        parent::__construct();
        $this->wallet = $wallet;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('Calculation started');
        $investments = Investment::where('status', '!=', 3)->get();
        $this->withProgressBar($investments, function ($investment) {
            (new InvestmentRepository($this->wallet))->process($investment);
        });
        $this->newLine();
        $this->info('Calculation end!');
    }
}
